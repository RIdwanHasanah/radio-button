package com.indonesia.ridwan.radiobutton;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity implements OnCheckedChangeListener {

    RadioGroup rg;
    RadioButton rb1,rb2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rg = (RadioGroup) findViewById(R.id.rg);
        rb1 = (RadioButton) findViewById(R.id.rbbakso);
        rb2 = (RadioButton) findViewById(R.id.rbmie);

        rg.setOnCheckedChangeListener(this);
    }

    public void onCheckedChanged(RadioGroup group,int checkedId){

        if (checkedId==R.id.rbbakso){

            Toast.makeText(this,"Anda Membekik Bakso Tahu",Toast.LENGTH_SHORT).show();
        }
        if (checkedId==R.id.rbmie){
            Toast.makeText(this,"ANda Memebeli Mie Ayam", Toast.LENGTH_SHORT).show();
        }

    }
}
